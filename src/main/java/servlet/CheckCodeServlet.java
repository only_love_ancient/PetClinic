package servlet;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@WebServlet( "/CheckCode")
public class CheckCodeServlet extends HttpServlet {
    private  static final  long serialVersionUID =1L;
    private int width=80;
    private  int height=20;
    private int codeCount=4;
    private  int x=16;
    private  int fontHeight=16;
    private int codeY=18;
    private final char[] codeSequence={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T',
            'U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException{
        BufferedImage Img=new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics g=Img.getGraphics();
        Random random=new Random();
        g.setColor(Color.white);
        g.fillRect(0,0,width,height);
        Font font=new Font("Times new Roman",Font.PLAIN,fontHeight);
        g.setColor(Color.BLACK);
        g.setFont(font);
        Color juneFont=new Color(153,204,102);
        g.setColor(juneFont);
        for (int i=0;i<130;i++){
            int x=random.nextInt(width);
            int y=random.nextInt(height);
            int x1=random.nextInt(16);
            int y1=random.nextInt(16);
            g.drawLine(x,y,x+x1,y+y1);
        }
        StringBuffer randomCode=new StringBuffer();
        for (int i=0;i<codeCount;i++){
            String strRand=String.valueOf(codeSequence[random.nextInt(36)]);
            g.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));
            g.drawString(strRand,(i+1)*x-4,codeY);
            randomCode.append(strRand);
        }
        HttpSession session=req.getSession();
        session.setAttribute("realcode",randomCode.toString());
        resp.setHeader("Pragma","no-chache");
        resp.setHeader("Cache-Control","no-chache");
        resp.setDateHeader("Expires",0);
        resp.setContentType("image/gif");
        ServletOutputStream sos=resp.getOutputStream();
        ImageIO.write(Img,"gif",sos);
        sos.flush();
        sos.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
