package servlet;

import model.Vet;
import unit.VetDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.alibaba.druid.sql.visitor.SQLEvalVisitorUtils.add;
import static jdk.nashorn.internal.objects.NativeString.search;

@WebServlet("/VetServlet?")
public class VetServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String m = request.getParameter("m");
        if("add".equals(m)) {
            add(request, response);
        } else if ("search".equals(m)) {
            search(request, response);
        }

    }
    private void search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            String vetName = request.getParameter("vetName");
            String specName = request.getParameter("specName");
            VetDao vetDAO = new VetDao();
            List<Vet> vets = VetDao.search(vetName,specName);
            if (vets.size() == 0) {
                request.setAttribute("msg", "没有找到相关医生信息");
                request.getRequestDispatcher("/vetsearchjp").forward(request, response);
            } else {
                request.setAttribute("vets", vets);
                request.getRequestDispatcher("/vetsearch_result.jsp").forward(request, response);
            }

        } catch (Exception e) {
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/vetsearch.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
