package model;

public class Pet {
    private int id;
    private String name;
    private String birthdate;
    private String photo;
    private int ownerId;

    public Pet() {
    }

    public Pet(int id, String name, String birthdate, String photo, int ownerId) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.photo = photo;
        this.ownerId = ownerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
}
