package model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private int id;
    private String role;
    private String name;
    private String pwd;
    private String tel;
    private String address;
    private List<Pet> pets = new ArrayList<Pet>();

    public int getId() {
        return id;
    }

    public User(int id, String role, String name, String pwd, String tel, String address) {
        this.id = id;
        this.role = role;
        this.name = name;
        this.pwd = pwd;
        this.tel = tel;
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }



}
