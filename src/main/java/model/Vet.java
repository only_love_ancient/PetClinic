package model;

import java.util.ArrayList;
import java.util.List;

public class Vet {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public Vet() {
    }

    public Vet(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getSpecs() {
        return null;
    }
}
